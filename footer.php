<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Makin\' Hay
 */
?>






	<!-- 	+main footer  -->
			<footer class="footer-main">
				<div class="container">
					<section class="footer-right">
						<a href="/" class="text-hide"><img src="/files/images/makin-hay-logo.png" alt="makin hay logo" class="u-full-width"  ></a>
						<a target="_blank" class="legal-link" href="/files/documents/RulesofEngagement.pdf">Rules of Engagement</a>
						<a target="_blank" href="/files/documents/ConditionsofUse.pdf" class="legal-link">Conditions of Use</a>
						<a target="_blank" href="http://www2.vermeer.com/vermeer/NA/en/N/privacy;jsessionid=542308B8C2C85BC7344F515660ACF2E4" class="legal-link">Privacy Policy</a>
					</section>
					<section class="footer-center">
						<form action="http://email.vermeer.com/t/r/s/jkdjiik/" method="POST" accept-charset="utf-8" autocomplete="on">
							<label for="signupemail">
								<p><strong>Vermeer News & Info</strong></p>
								<input id="signupemail" type="email" name="cm-jkdjiik-jkdjiik" required placeholder="Enter your Email Address">
								<button type="submit" class="button-primary" onClick="ga('envano.send', 'event', {eventCategory: 'Form', eventAction: 'Submit', eventLabel: 'Vermeer News & Info'})">Sign Up</button>
							</label>
							
						</form>
					</section>
					<section class="footer-left">
						<a href="#"><img src="/files/images/vermeer-logo@2x.png" alt="logo" class="u-full-width" /></a>
						<div>
							<p><small>Makin’ Hay aims to provide quality content that expands your knowledge of the hay and forage industry and helps you become more productive in the field. Makin’ Hay is produced by Vermeer Corporation.</small></p>
							<p class="padding-2x-top"><strong>&copy; 2015 Vermeer Corporation. All Rights Reserved.</strong></p>
							<a class="legal-link" target="_blank" href="/files/documents/RulesofEngagement.pdf">Rules of Engagement |</a> <a target="_blank" href="/files/documents/ConditionsofUse.pdf" class="legal-link">Conditions of Use |</a> <a target="_blank" href="http://www2.vermeer.com/vermeer/NA/en/N/privacy;jsessionid=542308B8C2C85BC7344F515660ACF2E4" class="legal-link">Privacy Policy</a>

							
						</div>
					</section>
				</div>
			</footer>
			<section>
			</section>
	<!-- 	-main footer  -->
		</div>
		
<?php wp_footer(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  //vermeer code
  ga('create', 'UA-59631102-1', 'auto');
  ga('send', 'pageview');
  //envano code 
  ga('create', 'UA-59616355-1', 'auto', {'name': 'envano'});
  ga('envano.send', 'pageview');

</script>

</body>
</html>
