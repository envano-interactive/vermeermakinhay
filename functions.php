<?php
/**
 * Makin\' Hay functions and definitions
 *
 * @package Makin\' Hay
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'vermeer_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function vermeer_setup() {
	//add featured post feature
	add_theme_support( 'post-thumbnails' ); 

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Makin\' Hay, use a find and replace
	 * to change 'vermeer' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'vermeer', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'vermeer' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'vermeer_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // vermeer_setup
add_action( 'after_setup_theme', 'vermeer_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function vermeer_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'vermeer' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'vermeer_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function vermeer_scripts() {
	//load style sheet
	wp_enqueue_style( 'vermeer-style', get_stylesheet_uri());

	//load js in footer
	wp_enqueue_script( 'vermeer-script', get_template_directory_uri() . '/js/all-min.js',  null, null, true);

}
add_action( 'wp_enqueue_scripts', 'vermeer_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


//load the sticky post on the home page
function home_featured_post($query) {
	if($query->is_home() && $query->is_main_query()) {
		$query->set( 'posts_per_page', 5 );
		$query->set( 'ignore_sticky_posts', 1 );
		$query->set( 'orderby', 'date' );
		$query->set( 'order', 'DESC' );
		$query->set( 'meta_query', array(
				array(
				'key' => 'make_feature_story',
				'value' => '1',
				'compare' => '=='		
				)
			) 
		);
	}
}
add_action( 'pre_get_posts', 'home_featured_post' ); 

function vermeer_featured_image($post_image_id, $size = "small", $url_only = false, $fallback = true) {
	$large_image_url = wp_get_attachment_image_src( $post_image_id, $size );
	if(empty($large_image_url) && $fallback) {
		$large_image_url[0] = "/files/images/placeholders/no-image-small.png";
	} 
	if($url_only) {
		return $large_image_url[0];
	} else {
		$alt = get_post_meta($post_image_id, '_wp_attachment_image_alt', true);
		return '<img src="' . $large_image_url[0] . '" alt="'. $alt . '"/>';
	}

}

add_filter( 'the_content_more_link', 'modify_read_more_link' );
function modify_read_more_link() {
 return '<a class="button margin-top-2x" href="' . get_permalink() . '" onClick="ga(\'envano.send\', \'event\', {eventCategory: \'Home\', eventAction: \'Click\', eventLabel: \'Keep Reading Link\'})">Keep Reading</a>';
}



// Add Shortcode for quote
/*
	<!-- 	post-quote-image -->
	<figure class="post-quote-image left">
		<img src="http://placehold.it/300x200" alt="Change Me" class="u-full-width" />
		<figcaption>
			<small><i>This is a caption for the photo above.</i></small>
		</figcaption>
	</figure>
	<!-- 	end post-quote-image -->
	<!-- 	post text quote  -->
	<blockquote class="post-quote-text right">
		"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
	</blockquote>
	<!-- 	end post text quote  -->
*/

function quote_shortcode( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(
 			'position' => 'right'
		), $atts )
	);
	
	// Code
	$content = "";
	if( have_rows('article-quotes') ) {
		the_row();
		$type = get_sub_field('text_or_image');
		
		
		if($type == "text")	{
			$text = get_sub_field('single-quote');
			$content = '<blockquote class="post-quote-text ' . esc_attr($atts['position']) . '">
				"' . $text . '"
			</blockquote>';
		} 
		if($type == "image") {
			
			$image = get_sub_field('image-qoute');
			$image_url = $image['sizes']['medium'];
			$image_caption = $image['caption'];
			$image_alt = $image['alt'];
			
			$content = '<figure class="post-quote-image ' . esc_attr($atts['position']) . '">
							<img src="' . $image_url . '" alt="'. $image_alt .'" class="u-full-width" />
						<figcaption>
								<small><i>' . $image_caption .  '</i></small>
							</figcaption>
						</figure>';
		}
		
		
	}
	
	return $content;

}
add_shortcode( 'quote', 'quote_shortcode' );
//end of shortcode function

//get avatar url
function get_avatar_url($get_avatar){
    preg_match("/src='(.*?)'/i", $get_avatar, $matches);
    return $matches[1];
}

			/*
		<!-- 	Comment		 -->
						<li>
							<img src="https://randomuser.me/api/portraits/med/men/52.jpg" class="u-full-width" alt="User">
							<div>
								<p><strong>Harry Washington</strong> <small>on January 8th</small></p>
								<p>
									Non eram nescius, Brute, cum, quae summis ingeniis exquisitaque doctrina philosophi Graeco sermone tractavissent, ea Latinis litteris mandaremus, fore ut hic noster labor in varias reprehensiones incurreret.
									Non eram nescius, Brute, cum, quae summis ingeniis exquisitaque doctrina philosophi Graeco sermone tractavissent, ea Latinis litteris mandaremus, fore ut hic noster labor in varias reprehensiones incurreret.
									Non eram nescius, Brute, cum, quae summis ingeniis exquisitaque doctrina philosophi Graeco sermone tractavissent, ea Latinis litteris mandaremus, fore ut hic noster labor in varias reprehensiones incurreret.
	
								</p>
							</div>
						</li>
*/

//custom function to style the comments 
function vemeer_custom_comment($comment, $args, $depth) {
	?> 
	<li <?php comment_class(); ?> id="comment<?php comment_ID(); ?>">
		<img src="<?php echo get_avatar_url(get_avatar( $comment, 100 )); ?>" alt="<?php comment_author(); ?>"></img>
		<div>
			<p><strong><?php comment_author(); ?></strong> <small>on <?php comment_date('F jS, Y'); ?></small></p>
			<p>
				<?php echo(get_comment_text()); ?>
			</p>
		</div>
	</li>
	<?php 
}//vemeer_custom_comment

//include any custom fields in the JSON response 

function wp_api_encode_acf($data,$post,$context){
    $customMeta = (array) get_fields($post['ID']);
    $data['meta'] = array_merge($data['meta'], $customMeta );
    return $data;
}
function wp_api_encode_acf_taxonomy($data,$post){
    $customMeta = (array) get_fields($post->taxonomy."_". $post->term_id );
    $data['meta'] = array_merge($data['meta'], $customMeta );
    return $data;
}
function wp_api_encode_acf_user($data,$post){
    $customMeta = (array) get_fields("user_". $data['ID']);
    $data['meta'] = array_merge($data['meta'], $customMeta );
    return $data;
}
//NOTE: Custom Fields can be added to the JSON response of posts, pages, etc.. but we are limiting it to json_prepare_attachment
//add_filter('json_prepare_post', 'wp_api_encode_acf', 10, 3);
//add_filter('json_prepare_page', 'wp_api_encode_acf', 10, 3);
add_filter('json_prepare_attachment', 'wp_api_encode_acf', 10, 3);
//add_filter('json_prepare_term', 'wp_api_encode_acf_taxonomy', 10, 2);
//add_filter('json_prepare_user', 'wp_api_encode_acf_user', 10, 2);

//end of custom fields code 


//add categories to images 
function wptp_add_categories_to_attachments() {
    register_taxonomy_for_object_type( 'category', 'attachment' );
}
add_action( 'init' , 'wptp_add_categories_to_attachments' );


add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}

add_image_size( 'gallery_image_small', 500, 500, false ); 


