<?php
/**
 * The header for our theme.
 *
 * 
 *
 * @package Makin\' Hay
 */
?>
<!DOCTYPE html>

<html <?php language_attributes(); ?> ng-app="makinHay">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, user-scalable=no, minimal-ui">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<meta name="description" content="Stay up to date with posts on the agriculture industry. Building your business, expanding your fleet, job stories, and more - you’ll find it on Makin’ Hay.">
<?php if(is_home()) {  ?>
<!-- +facebook Open Graph  -->
 <meta property="og:image" content="<?php echo esc_url( home_url( '/' ) ); ?>/wp-content/uploads/2015/02/VR_CenterSplitter.jpg">  
 <?php } ?>
<!-- -facebook Open Graph-->

<!-- +shortcut icons  -->
<meta name="mobile-web-app-capable" content="yes">

<link rel="shortcut icon" sizes="196x196" href="/files/ico/196x196.png">
<link rel="apple-touch-icon" href="/files/ico/60x60.png">
<link rel="apple-touch-icon" sizes="76x76" href="/files/ico/76x76.png">
<link rel="apple-touch-icon" sizes="120x120" href="/files/ico/120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/files/ico/152x152.png">
<link rel="shortcut icon" type="image/ico" href="/files/ico/favicon.ico"><!-- -shortcut icons  -->
<?php wp_head(); ?>
<!--[if lte IE 8]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="/files/js/source/vendor/respond.src.js" charset="utf-8"></script>
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<![endif]-->
<!--[if gte IE 9]>
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<![endif]-->
<!--[if !IE]> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<!-- <![endif]-->
</head>

<body <?php body_class(); ?>>
	<!-- 	+main navigation  -->
		<nav class="top-main-slide-nav" >
			
			<div class="left">
			<?php 
			?>
				<h5 class="text-caps">SELECT A Story Category</h5>
				<ul>
				<?php 
						    $args = array(
							'show_option_all'    => '',
							'orderby'            => 'count',
							'order'              => 'ASC',
							'style'              => 'list',
							'show_count'         => 0,
							'hide_empty'         => 0,
							'use_desc_for_title' => 1,
							'child_of'           => 0,
							'feed'               => '',
							'feed_type'          => '',
							'feed_image'         => '',
							'exclude'            => '1',
							'exclude_tree'       => '',
							'include'            => '',
							'hierarchical'       => 1,
							'title_li'           => __( '' ),
							'show_option_none'   => __( '' ),
							'number'             => null,
							'echo'               => 0,
							'depth'              => 0,
							'current_category'   => 0,
							'pad_counts'         => 0,
							'taxonomy'           => 'category',
							'walker'             => null
						    );
						    echo wp_list_categories( $args ); 
					
				?>
				</ul>
				
			</div>
			<div class="right">
				<h5 class="text-caps">Popular Stories</h5>
				<?php 
					$args = array(
						'posts_per_page' => 5,
						'ignore_sticky_posts'  => 1,
						'orderby' => 'date',
						'order' => 'DESC',
						'meta_query' => array(
							array(
							'key' => 'make_feature_story',
							'value' => '1',
							'compare' => '=='		
							)
						) 
					);
					$featured_posts_query = new WP_Query( $args );
					if($featured_posts_query->have_posts()) {
				?>
					<ul>
						<?php while($featured_posts_query->have_posts() ) { 
							$featured_posts_query->the_post();
						?>
						<li>
							<a href="<?php the_permalink(); ?>">
								<strong><?php the_title(); ?></strong>
								<small class="text-caps"><?php the_time('F Y'); ?></small>
							</a>
						</li>
						<?php } ?>
					</ul>
				<?php } 
					
					wp_reset_postdata();
				?>
			</div>
			<a class="icon icon-circle-up" data-js-trigger="MainMenu" href="#"> Close</a>
		</nav>
		<nav class="side-search-bar padding-2x-all"  ng-controller="SearchBarController">
			<a class="icon icon-circle-left" href="#" data-js-trigger="SearchBar"></a>
			<form ng-submit="DoSearch(term)" novalidate name="SearchForm" class="u-full-width">
				<input type="text" ng-model="term" class="u-full-width" placeholder="Search" ng-required="true">
			</form>
			<section>
				<header ng-show="results.length">
					<h5 ng-if="results.length == 1">{{results.length}} Result</h5>
					<h5 ng-if="results.length > 1">{{results.length}} Results</h5>
				</header>
				<header ng-if="results.length == 0">
					<h5>No Results</h5>
				</header>
				<ul>
					<li ng-repeat="post in results" >
						<a ng-href="{{post.link}}">
							<strong>{{post.title}}</strong>
							<small>{{post.date | date : 'MMMM, y'}}</small>
						</a>
					</li>
				</ul>
			</section>
		</nav>
	
	
<div class="body-wrapper" >
		
			<header class="nav-main u-cf u-full-width" <?php  if(is_single()) { echo('data-js-trigger="minimal-header"'); } ?>>
				<div class="container">
					
					<nav class="vertical nav-left">
						<a href="#menu" onClick="ga('envano.send', 'event', {eventCategory: 'Menu', eventAction: 'Click', eventLabel: 'Main Menu'})" data-js-trigger="MainMenu" class="center icon icon-menu menu-trigger js-open-menu"></a>
						<ul class="center ul-reset">
							<li>
								<a href="#more" onClick="ga('envano.send', 'event', {eventCategory: 'Menu', eventAction: 'Click', eventLabel: 'Main Menu'})" class="js-open-menu" data-js-trigger="MainMenu">
									<span class="icon icon-menu large-icon js-open-menu"></span> 
								</a>
							</li>
							<li>
								<a href="#more" onClick="ga('envano.send', 'event', {eventCategory: 'Menu', eventAction: 'Click', eventLabel: 'Main Menu'})" data-js-trigger="MainMenu" class="js-open-menu">
									More Stories
								</a>
							</li>
							<li>
								<a href="/gallery/"> Photos & Videos</a>
							</li>
						</ul>
		
					</nav>
					<h1 class="nav-center margin-none vertical">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="center" rel="<?php bloginfo( 'name' ); ?>"> </a>
					</h1>
					<nav class="nav-right vertical">
						<div class="center">
							<ul class="ul-reset">
								<li>
									Connect With Us
									<a href="https://www.facebook.com/vermeeragriculture"  target="_blank" class="icon icon-facebook2 text-hide"></a>
									<a href="https://twitter.com/vermeerag" target="_blank" class="icon icon-twitter2 text-hide"></a>
									<a href="mailto:makinhay@vermeer.com"  target="_blank" class="icon icon-mail text-hide"></a>
								</li>
								<li>
									<a target="_blank" href="http://vermeer.com">Vermeer.com</a>
								</li>
							</ul>
							<button data-js-trigger="SearchBar" class="u-pull-right margin-none search-bar" onClick="ga('envano.send', 'event', {eventCategory: 'Menu', eventAction: 'Click', eventLabel: 'Search Bar'})"><span class="hide-on-mobile search-bar">Search Stories</span> <span class="icon icon-search search-bar"></span></button>
						</div>
					</nav>
				</div>
			</header>
	<!-- 	-main navigation  -->














			
