<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Makin\' Hay
 */


get_header(); ?>



	<!-- 	+main header/hero  -->
			<?php 
				$args = array(
				'posts_per_page' => 1,
				'post__in'  => get_option( 'sticky_posts' ),
				'ignore_sticky_posts' => 1
				);
				$featured_post_query = new WP_Query( $args );
			?>
			
			<header class="hero-main" data-js-tag="adjust-hero">
			<?php
				//if there are posts
				if ( $featured_post_query->have_posts() ) {
					/* Start the Loop */
					while ( $featured_post_query->have_posts() ) {
						$featured_post_query->the_post();
						$featured_image = vermeer_featured_image(get_post_thumbnail_id(),$size = "medium", $url_only = false);
						$featured_imageURL = vermeer_featured_image(get_post_thumbnail_id(),$size = "medium", $url_only = true);
						
			?>			
				<figure>
					<a href="<?php echo the_permalink(); ?>" onClick="ga('envano.send', 'event', {eventCategory: 'Home', eventAction: 'Click', eventLabel: 'Featured Story Hero'})">
						<?php echo $featured_image ?> 
						
						<div class="cover-image" style="background-image: url(<?php echo $featured_imageURL ?>)"></div>
						<div class="overlay-color"></div>
						<figcaption>
							<h6><span class="icon icon-star-full"></span> FEATURED STORY</h6>
								<h2><span><?php the_title() ?></span></h2>
						</figcaption>
					</a>
				</figure>
			<?php
					}  
				}
			wp_reset_postdata();
			?>
				<aside class="padding-4x-all">
					<!-- Stories -->
					<section>
						<h6><span class="icon icon-drawer"></span> Popular Stories</h6>
						<div class="overflow-posts">
						<?php if ( have_posts() ) { ?>
						
						<ul class="posts">
							<?php while ( have_posts() ) { 
								the_post(); 
								$featured_image = vermeer_featured_image(get_post_thumbnail_id(), $size = "small" ,$url_only = false);
								$featured_imageURL = vermeer_featured_image(get_post_thumbnail_id(), $size = "small", $url_only = true);
								
								
							?>
							
							<li class="post-article-story"> 
								<figure>
									<a href="<?php echo the_permalink(); ?>">
										<?php echo($featured_image); ?>
										<div class="cover-image" style="background-image: url(<?php echo $featured_imageURL ?>)"></div>
										<div class="overlay-color"></div>

										<figcaption>
											<p><strong><?php the_title(); ?></strong></p>
										</figcaption>
									</a>
								</figure>
							</li>
							<?php } ?>
 						</ul>
						<?php } 
							wp_reset_postdata();
						?>
						</div>
						<hr class="hr-light">
					</section><!-- Stories -->
					<footer>
						<!-- Sign up for updates -->
						<section ng-controller="FBFeedController">
							<blockquote class="padding-1x-all fb-feed box-shadow-small box-border-1x-light">
								<p>{{feed |limitTo:250}} <span ng-if="feed.length > 249">...</span></p>
								<footer><a target="_blank" href="https://www.facebook.com/vermeeragriculture"><span class="icon icon-facebook2 text-inherit"></span> Follow Vermeer Agriculture <span class="text-light">on Facebook</span></a></footer>
							</blockquote>
							<h6><span class="icon icon-envelop"></span> VERMEER NEWS & INFO</h6>
							<form class="form-sign-up"  action="http://email.vermeer.com/t/r/s/jkdjiik/" method="POST" accept-charset="utf-8" autocomplete="on">
								<label for="signupemail">
									<input id="signupemail" type="email" name="cm-jkdjiik-jkdjiik" required placeholder="Enter your Email Address">
									<button type="submit" class="button-primary" onClick="ga('envano.send', 'event', {eventCategory: 'Form', eventAction: 'Submit', eventLabel: 'Vermeer News & Info'})">Sign Up</button>
								</label>
								
							</form>
						</section><!-- Sign up for updates -->
						<a href="<?php bloginfo('rss2_url'); ?>" target="_blank"><span class="icon icon-feed3"></span> Add to your RSS feed</a>
					</footer>
	
				</aside>
				<small class="scroll-for-more"><!-- Scroll to view more --><span class="icon icon-circle-down"></span></small>
			</header>
	<!-- 	-main header/hero  -->

	<!-- 	+main content  -->
			<article class="content-main">
				<div class="container row">
					<?php 
					if ( $featured_post_query->have_posts() ) {		
						$featured_post_query->the_post();	
								
					?>
					<section class="eight columns">
						<p><strong><?php the_date('F Y') ?></strong></p>
						<?php the_content(); ?>
					</section>
 					<?php 
					//this is for the side image/text quotes
					if( have_rows('article-quotes') ) { 
						the_row();		
						$type = get_sub_field('text_or_image');
					?>
					<aside class="four columns">
						<?php 
							//if the first a text qoute 
							if($type == "text")	{
								$text = get_sub_field('single-quote');
								 
						?>
							<blockquote class="post-quote-text">
								"<?php echo($text) ?>"
							</blockquote>
						<?php 
							//else if its an image qoute
							} else if($type == "image") {
								$image = get_sub_field('image-qoute');
								//print_r($image);
								$image_url = $image['sizes']['medium'];
								$image_caption = $image['caption'];
								$image_alt = $image['alt'];
						?>
	 						<figure class="post-quote-image">
								<img src="<?php echo($image_url) ?>" alt="<?php echo $image_alt ?>" class="u-full-width" />
								<figcaption>
									<?php if(!empty($image_caption)) { ?>
									<small><i><?php echo($image_caption); ?></i></small>
									<?php } ?>
								</figcaption>
							</figure>
						<?php } ?>
						
					</aside>
					<a href="<?php the_permalink() ?>" class="button">Keep Reading</a>
					<?php 
						} 
					}
					?>
				</div>
			</article>
	<!-- 	-main content  -->




<?php /* get_sidebar(); */ ?>
<?php get_footer(); ?>




