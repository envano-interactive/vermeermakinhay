<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Makin\' Hay
 */
?>
 			<header class="page-header">
				<?php
					the_title( '<h5 class="page-title text-center text-caps ">', '</h5>' );
				?>
			</header><!-- .page-header -->

		<?php the_content(); ?>
		<?php 
			$field = get_field_object('set_as_gallery_page');
			$value = get_field('set_as_gallery_page');
			 
			if(isset($value[0])) { 
			?>
				<div class="main-gallery-wrapper"  ng-controller="GalleryController">
					<form name="filterGallery">
 						<ul class="row">
							<div class="three columns">
							  <?php 
									$args = array(
										'type'                     => 'post',
	  									'orderby'                  => 'name',
	 									'hide_empty'               => 0,
										'hierarchical'             => 1,
										'exclude'                  => '1',
										'taxonomy'                 => 'category',
									
									); 							  
	
									$categories = get_categories( $args ); 
									if(count($categories)) { ?>
										 <select class="u-full-width" name="category" id="category">
											 <option value=" ">Select Category</option>
										<?php 
										foreach($categories as $category) { ?>
											<option value="<?php echo $category->cat_ID ?>"><?php echo $category->name  ?></option>
									<?php 
									}
										echo('</select>');
									}
								?>
							</div>
							<div class="u-pull-right three columns">
							  <select class="u-full-width" name="category" id="category" ng-model="direction">
								  <option  value="">Newest</option>
								  <option value="reverse">Oldest</option>
							  </select>
							</div>
						</ul>
					</form>
					<div class="main-gallery" ng-init="loadImages(currentPage, limit)">
						<a ng-repeat="(index, image) in galleryItems" href="{{image.source}}">
							<img ng-src="{{image.attachment_meta.sizes.thumbnail.url}}" alt="{{image.title}}"  imageonload="{{index + 1}}"/>
						</a>
	 				</div>	
	 				<button ng-hide="done" class="button-primary" onClick="ga('envano.send', 'event', {eventCategory: 'Gallery', eventAction: 'Click', eventLabel: 'Load More'})" ng-click="loadImages(currentPage, limit)">Load More</button>
				</div>
			<?php } ?>
