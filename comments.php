<?php
/**
 * The template for displaying comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package Makin\' Hay
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<article class="post-comments-wrapper">
	<div class="container">
	<?php // You can start editing here -- including this comment! ?>

	<?php if ( have_comments() ) { ?>
		<header>
			<h5>
			<?php
				printf( _nx( 'One Comment', '%1$s Comments', get_comments_number(), NULL, NULL ),
					number_format_i18n( get_comments_number() ), NULL );
			?>
			</h5>
		</header>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-above" class="comment-navigation" role="navigation">
			<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'vermeer' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'vermeer' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'vermeer' ) ); ?></div>
		</nav><!-- #comment-nav-above -->
		<?php endif; // check for comment navigation ?>

		<ul class="post-comments">
			<?php
				wp_list_comments( array(
					'style'      => 'ul',
 					'callback' => 'vemeer_custom_comment'
				) );
			?>
		</ul><!-- .post-comments -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-below" class="comment-navigation" role="navigation">
			<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'vermeer' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'vermeer' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'vermeer' ) ); ?></div>
		</nav><!-- #comment-nav-below -->
		<?php endif; // check for comment navigation ?>

	<?php } else { // have_comments() ?>
			<header class="no-comments">
				<h5>No Comments</h5>
			</header>
			
	<?php
		}
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'vermeer' ); ?></p>
	<?php endif; ?>
		<footer>
		<?php 
			$args = array(
				'comment_notes_before' => NULL,
				'fields' => apply_filters(
					'comment_form_default_fields', array(
						'author' =>'<div class="row">' . '<div class="six columns"><label for="author">Name</label><input id="author" class="u-full-width" placeholder="John Smith" name="author" type="text" value="' .
							esc_attr( $commenter['comment_author'] ) . '"' . $aria_req . ' />'.
  							'</div>'
							,
						'email'  => '<div class="six columns">' . '<label for="email">Email</label><input  class="u-full-width" id="email" placeholder="JohnSmith@example.com" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
							'"' . $aria_req . ' />'  .
 							 '</div></div>'
 					)
				),
				'comment_field' => '<label for="comment">Comment</label>' .
 					'<textarea id="comment" name="comment" class="u-full-width" placeholder="What’s on your mind..." aria-required="true"></textarea>',
			    'comment_notes_after' => '<input class="button-primary" type="submit" value="Submit" name="submit"> <input type="reset" class="button" value="Clear">',
			    'title_reply' => '<h6>Add Comment</h6>'
			);
				
			comment_form($args); 
			
		?>
		</footer>
	</div>
</article><!-- #comments -->
