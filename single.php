<?php
/**
 * The template for displaying all single posts.
 *
 * @package Makin\' Hay
 */

get_header(); ?>

	<article class="content-main">
 
		<?php while ( have_posts() ) {
			 	
			 	the_post(); 
				get_template_part( 'content', 'single' );	
					 
		?>
	</article>
 <!-- Gallery -->
 	<?php
	 //the gallery 
	if(get_field('attach_gallery')) {
		//to create the gallery html
		$galleryHtml = "";
		//get the value of the linked gallery
		$linkedgallery = get_field('attach_gallery');
		//make sure its not empty
		if(!empty($linkedgallery)) {
			//create the gallery wrapper
			$galleryHtml .= '<article class="post-gallery bg-black overflow">
								<div class="container">
									<header>';
			//since each gallery is a custom post type then its treated like a post loop
			//IMPORTANT: the single item must be $post in order to be used with setup_postdate();
			foreach($linkedgallery as $post) {
				//setup the gallery post data
				setup_postdata($post);
				//get all the images 
				$images = get_field('images');
				//get videos
				//if its not empty
 				if(!empty($images)) {
 					$galleryHtml .= '<h6><a href="#" data-js-tag="showImages"><span class="icon icon-images"></span> Images</a></h6>';
				
				}
				//if there are videos
				if(have_rows('videos')) {
					//while there are video rows
					$galleryHtml .= '<h6><a href="#"  class="active"  data-js-tag="showVideos"><span class="icon icon-youtube"></span> Videos</a></h6>';
					$videos = array();
					$counter = 0;
					while(have_rows('videos')) {
						//set up the video data row
						the_row();
						//get the video object
						//by default, the row contains the full embeded iframe html
						$video = get_sub_field('video');
						//so lets use regex to grab just anything inside the source
						preg_match('/src="(.+?)"/', $video, $videosrc); //this returns the array of $videosrc containing the source of the video
						//print_r($videosrc[1]);
						//if this is a youtube video then grab the id
						if(strpos($videosrc[1], 'youtube') || strpos($videosrc[1], 'youtu')) {
							//http://stackoverflow.com/questions/3392993/php-regex-to-get-youtube-video-id
							preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $videosrc[1], $youtubeId);
							//print_r($youtubeId);
							//add it to the videos array
  							$videos[$counter]['type'] = "youtube";
							$videos[$counter]['id'] = $youtubeId[1];
							$videos[$counter]['src'] = $videosrc[1];
							$videos[$counter]['real_url'] = "https://www.youtube.com/watch?v=" . $youtubeId[1];
							//http://stackoverflow.com/questions/2068344/how-do-i-get-a-youtube-video-thumbnail-from-the-youtube-api
							$videos[$counter]['poster'] = 'http://img.youtube.com/vi/' . $youtubeId[1] . '/hqdefault.jpg';
						} elseif(strpos($videosrc[1], 'vimeo') || strpos($videosrc[1], 'vimeo')) {
							//http://stackoverflow.com/questions/10488943/easy-way-to-get-vimeo-id-from-a-vimeo-url
							preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $videosrc[1], $vimeo);
							//print_r($vimeo);
  							$videos[$counter]['type'] = "vimeo";
							$videos[$counter]['id'] = $vimeo[5];
							$videos[$counter]['src'] = $videosrc[1];
							$videos[$counter]['real_url'] = "http://vimeo.com/" . $vimeo[5];
							$thumbnailsRequest = file_get_contents('http://vimeo.com/api/v2/video/' . $vimeo[5] . '.php');
							$thumbnails = unserialize($thumbnailsRequest);
							//print_r($thumbnails);
							$videos[$counter]['poster'] = $thumbnails[0]['thumbnail_large'];
							
						}
						$counter ++;
						
					}
					//print_r($videos);
				}
				//close the header
				$galleryHtml .= '</header>';
				//build the image gallery
				//print_r($images);
				
				if(!empty($images)) {
					//images wrapper
					$galleryHtml .= '<section class="bounceInLeft animated images"><div class="single-post-gallery">';	
					foreach($images as $image) {
						//print_r($image);
						$imgTitle =  $image['title'];
						$imgAlt = $image['alt'];
						$imgCap = $image['caption'];
						$altText = " ";
						if(!empty($imgAlt)) {
							$altText = $imgTitle;
						} else if(!empty($imgCap)) {
							$altText = $imgCap;
						} else if(!empty($imgTitle)) {
							$altText = $imgTitle;
						}
						
						$imgLarge = $image['sizes']['large'];
						$imgSmall = $image['sizes']['thumbnail'];
						$galleryHtml .= "<div>";
						$galleryHtml .= '<a href="' . $imgLarge . '">';
						$galleryHtml .= '<img class="u-full-width" src="' . $imgSmall . '" alt="' . $altText . '" />';
						$galleryHtml .= '</a>';
						$galleryHtml .= "</div>";
					}
					$galleryHtml .= '</div></section>';
				}
				
				
				//build the video gallery
 				if(!empty($videos)) {
					//images wrapper
					$galleryHtml .= '<section class="bounceInRight animated videos"><div class="single-post-gallery">';	
					foreach($videos as $video) {
						//print_r($video);
 						$galleryHtml .= '<a href="' . $video['real_url'] . '">';
						$galleryHtml .= '<img class="u-full-width" src="' . $video['poster']  . '" alt="Video Image" />';
						$galleryHtml .= '</a>';
 					}
					$galleryHtml .= '</ul></section>';
				}
				
				
				
				
				
				
				
				$galleryHtml .= '</div>';
				$galleryHtml .= '</article>';
				wp_reset_postdata(); //IMPORTANT! Otherwise it wont load the rest of the page correctly
			}
			echo($galleryHtml);
		}
	} //end of gallery stuff	
				
	// If comments are open or we have at least one comment, load up the comment template
	if ( comments_open() || get_comments_number() ) {
		comments_template();
	}
	the_related_post();
	?>
		<?php } // end of the loop. ?>
		
<!-- end of content-main -->
<?php //get_sidebar(); ?>

<?php get_footer(); ?>
<script>
	$('.single-post-gallery').justifiedGallery({
		rowHeight : 200, 
		margins: 15,
		lastRow: 'justify',
					
	}).on('jg.complete', function(){
		$('.single-post-gallery a').swipebox();
	});

</script>

