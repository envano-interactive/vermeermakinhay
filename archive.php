<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Makin\' Hay
 */

get_header(); ?>

	<article class="content-main">
		<section class="container">
		<?php if ( have_posts() ) { ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h5 class="page-title text-center text-caps ">', '</h5>' );
				?>
			</header><!-- .page-header -->
 			<ul class="posts">	
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) {
				 	the_post(); 
					$featured_image = vermeer_featured_image(get_post_thumbnail_id(), $size = "small" ,$url_only = false);
					$featured_imageURL = vermeer_featured_image(get_post_thumbnail_id(), $size = "small", $url_only = true);
			?>
				<li> 
					<figure>
						<a href="<?php the_permalink() ?>">
							<?php echo($featured_image); ?>
							<div class="cover-image" style="background-image: url(<?php echo $featured_imageURL ?>)"></div>
							<div class="overlay-color"></div>
							<figcaption>
								<p><strong class="text-white"><?php the_title() ?></strong></p>
							</figcaption>
						</a>
					</figure>
				</li>
									 
			<?php } ?>
			</ul>
 		<?php } else { ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php } ?>
		</section>
	</article><!-- #primary -->

<?php // get_sidebar(); ?>
<?php get_footer(); ?>
