<?php
/**
 * @package Makin\' Hay
 */
 
 $featured_imageURL = vermeer_featured_image(get_post_thumbnail_id(),$size = "medium", $url_only = true, $fallback = false);

?>
		<aside class="social-share-bar">
			<ul>
 				<li class="facebook">
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo (get_permalink()) ?>" class="icon icon-facebook" onClick="ga('envano.send', 'event', {eventCategory: 'Social Share', eventAction: 'Click', eventLabel: 'Facebook'})"></a>
				</li>
				<li class="twitter">
					<a href="https://twitter.com/home?status=<?php echo (get_permalink()) ?>" class="icon icon-twitter" onClick="ga('envano.send', 'event', {eventCategory: 'Social Share', eventAction: 'Click', eventLabel: 'Twitter'})"></a>
				</li>
				<li class="google-plus">
					<a href="https://plus.google.com/share?url=<?php echo (get_permalink()) ?>" class="icon icon-google-plus" onClick="ga('envano.send', 'event', {eventCategory: 'Social Share', eventAction: 'Click', eventLabel: 'Google Plus'})"></a>
				</li>
				<li class="linkedin"> 
					<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo (get_permalink()) ?>&title=<?php the_title() ?>&summary=&source=" class="icon icon-linkedin2" onClick="ga('envano.send', 'event', {eventCategory: 'Social Share', eventAction: 'Click', eventLabel: 'LinkedIn'})"></a>
				</li>
				<li class="pinterest">
					<a href="https://pinterest.com/pin/create/button/?url=&media=<?php echo (get_permalink()) ?>&description=" class="icon icon-pinterest" onClick="ga('envano.send', 'event', {eventCategory: 'Social Share', eventAction: 'Click', eventLabel: 'Pinterest'})"></a>
				</li>
			</ul>
		</aside>
<section class="container" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<!-- featured image and date	 -->
	<?php 
		$featuredImagePos = get_field_object('featured_image_position');
		$featuredImagePos = $featuredImagePos['value'];
	?> 
	<?php if($featured_imageURL) { ?>
	<img  src="<?php echo($featured_imageURL) ?>" alt="<?php the_title() ?>"  class="featured-image <?php echo($featuredImagePos) ?>" />
	<?php } ?>
	<div class="post-info">
		<h4><?php the_title() ?></h4>
		<p><strong><?php the_time('F Y'); ?></strong></p>
 	</div>
 	<!--  Post main content	 -->
 	<?php the_content(); ?>
</section>
