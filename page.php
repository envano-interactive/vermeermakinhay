<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Makin\' Hay
 */

get_header(); ?>

<article class="content-main" id="post-<?php the_ID(); ?>">
	<section class="container">

			<?php while ( have_posts() ) { the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
/*
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
*/
				?>

			<?php } // end of the loop. ?>

	</section>
</article>
 <?php get_footer(); ?>
 
<script>
	
/*
	setTimeout(function(){
		$('.main-gallery-wrapper > .main-gallery').show();
		$('.main-gallery-wrapper > .main-gallery').justifiedGallery({
		rowHeight : 200, 
		margins: 10,
		lastRow: 'justify',
					
	}).on('jg.complete', function(){
		$('.main-gallery-wrapper > .main-gallery a').swipebox();
	});
	}, 1500);
*/
	
</script>
