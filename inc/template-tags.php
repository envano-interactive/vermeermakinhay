<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Makin\' Hay
 */
 
if ( ! function_exists( 'the_posts_navigation' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 */
function the_posts_navigation() {
	// Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}
	?>
	<nav class="navigation posts-navigation" role="navigation">
		<h2 class="screen-reader-text"><?php _e( 'Posts navigation', 'vermeer' ); ?></h2>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( 'Older posts', 'vermeer' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts', 'vermeer' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( !function_exists( 'the_related_post' ) ) :
/**
 * Display navigation to next/previous post when applicable.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 */
function the_related_post() {
 		//get the categories for this post 
		$category = get_the_category();
		$currentPostId = get_the_ID();
		$html = "";
 		global $post;
		//only get the first category if it exist
		if($category[0]) {
			$catID = $category[0]->cat_ID;
			//set up the query
			$args = array( 'numberposts' => 6, 'category' => $catID, 'exclude' => $currentPostId);
			$relatedPosts = get_posts($args);
			$totalPosts = count($relatedPosts);
			$term = "Stories";
			if($totalPosts == 1) {
				$term = "Story";
			}
 			if(!empty($relatedPosts)) {
				$html .= '<article class="post-related-wrapper bg-black">
							<div class="container">';
				
				$html .= '<header><h6>' . $totalPosts .  ' Other ' . $term . ' from this Category</h6></header>';
				$html .= '<ul class="posts">';
				foreach($relatedPosts as $post) {
					setup_postdata($post);
					$featured_image = vermeer_featured_image(get_post_thumbnail_id(), $size = "small" ,$url_only = false);
					$featured_imageURL = vermeer_featured_image(get_post_thumbnail_id(), $size = "small", $url_only = true);
					$html .= '<li class="post-article-story"> 
								<figure>
									<a href="' . get_the_permalink() . '">
										' . $featured_image . ' 
										<div class="cover-image" style="background-image: url(' . $featured_imageURL . ')"></div>
										<div class="overlay-color"></div>
										<figcaption>
											<p><strong>' . get_the_title() . '</strong></p>
										</figcaption>
									</a>
								</figure>
							</li>';
  				}
  				$html .= '</ul>';
  				$html .= 	'</div>
  						</article>';
			}
			wp_reset_postdata();
			echo $html;
		}
		
		
		
		
		
}
endif;

if ( ! function_exists( 'vermeer_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function vermeer_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		_x( 'Posted on %s', 'post date', 'vermeer' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		_x( 'by %s', 'post author', 'vermeer' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>';

}
endif;

if ( ! function_exists( 'vermeer_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function vermeer_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' == get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( __( ', ', 'vermeer' ) );
		if ( $categories_list && vermeer_categorized_blog() ) {
			printf( '<span class="cat-links">' . __( 'Posted in %1$s', 'vermeer' ) . '</span>', $categories_list );
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', __( ', ', 'vermeer' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . __( 'Tagged %1$s', 'vermeer' ) . '</span>', $tags_list );
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( __( 'Leave a comment', 'vermeer' ), __( '1 Comment', 'vermeer' ), __( '% Comments', 'vermeer' ) );
		echo '</span>';
	}

	edit_post_link( __( 'Edit', 'vermeer' ), '<span class="edit-link">', '</span>' );
}
endif;

if ( ! function_exists( 'the_archive_title' ) ) :
/**
 * Shim for `the_archive_title()`.
 *
 * Display the archive title based on the queried object.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 *
 * @param string $before Optional. Content to prepend to the title. Default empty.
 * @param string $after  Optional. Content to append to the title. Default empty.
 */
function the_archive_title( $before = '', $after = '' ) {
	if ( is_category() ) {
		$title = sprintf( __( 'Category: %s', 'vermeer' ), single_cat_title( '', false ) );
	} elseif ( is_tag() ) {
		$title = sprintf( __( 'Tag: %s', 'vermeer' ), single_tag_title( '', false ) );
	} elseif ( is_author() ) {
		$title = sprintf( __( 'Author: %s', 'vermeer' ), '<span class="vcard">' . get_the_author() . '</span>' );
	} elseif ( is_year() ) {
		$title = sprintf( __( 'Year: %s', 'vermeer' ), get_the_date( _x( 'Y', 'yearly archives date format', 'vermeer' ) ) );
	} elseif ( is_month() ) {
		$title = sprintf( __( 'Month: %s', 'vermeer' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'vermeer' ) ) );
	} elseif ( is_day() ) {
		$title = sprintf( __( 'Day: %s', 'vermeer' ), get_the_date( _x( 'F j, Y', 'daily archives date format', 'vermeer' ) ) );
	} elseif ( is_tax( 'post_format' ) ) {
		if ( is_tax( 'post_format', 'post-format-aside' ) ) {
			$title = _x( 'Asides', 'post format archive title', 'vermeer' );
		} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
			$title = _x( 'Galleries', 'post format archive title', 'vermeer' );
		} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
			$title = _x( 'Images', 'post format archive title', 'vermeer' );
		} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
			$title = _x( 'Videos', 'post format archive title', 'vermeer' );
		} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
			$title = _x( 'Quotes', 'post format archive title', 'vermeer' );
		} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
			$title = _x( 'Links', 'post format archive title', 'vermeer' );
		} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
			$title = _x( 'Statuses', 'post format archive title', 'vermeer' );
		} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
			$title = _x( 'Audio', 'post format archive title', 'vermeer' );
		} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
			$title = _x( 'Chats', 'post format archive title', 'vermeer' );
		}
	} elseif ( is_post_type_archive() ) {
		$title = sprintf( __( 'Archives: %s', 'vermeer' ), post_type_archive_title( '', false ) );
	} elseif ( is_tax() ) {
		$tax = get_taxonomy( get_queried_object()->taxonomy );
		/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
		$title = sprintf( __( '%1$s: %2$s', 'vermeer' ), $tax->labels->singular_name, single_term_title( '', false ) );
	} else {
		$title = __( 'Archives', 'vermeer' );
	}

	/**
	 * Filter the archive title.
	 *
	 * @param string $title Archive title to be displayed.
	 */
	$title = apply_filters( 'get_the_archive_title', $title );

	if ( ! empty( $title ) ) {
		echo $before . $title . $after;
	}
}
endif;

if ( ! function_exists( 'the_archive_description' ) ) {
/**
 * Shim for `the_archive_description()`.
 *
 * Display category, tag, or term description.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 *
 * @param string $before Optional. Content to prepend to the description. Default empty.
 * @param string $after  Optional. Content to append to the description. Default empty.
 */
function the_archive_description( $before = '', $after = '' ) {
	$description = apply_filters( 'get_the_archive_description', term_description() );

	if ( ! empty( $description ) ) {
		/**
		 * Filter the archive description.
		 *
		 * @see term_description()
		 *
		 * @param string $description Archive description to be displayed.
		 */
		echo $before . $description . $after;
	}
}
}

if ( ! function_exists( 'the_post_gallery' ) ) {

/**
	Post gallery	
	
 */
	function the_post_gallery() {
		//make sure the field exists 
		if(get_field('attach_gallery')) {
			//get the value of the linked gallery
			$linkedgallery = get_field('attach_gallery');
			//make sure its not empty
			if(!empty($linkedgallery)) {
				//since each gallery is a custom post type then its treated like a post loop
				//IMPORTANT: the single item must be $post in order to be used with setup_postdate();
				foreach($linkedgallery as $post) {
					//setup the gallery post data
					setup_postdata($post);
 					//get all the images 
					$images = get_field('images');
					print_r($post);
					//get videos
					//if its not empty 
					if(have_rows('videos')) {
						//while there are video rows
						while(have_rows('videos')) {
							//set up the video data row
							the_row();
							//get the video object
							//by default, the row contains the full embeded iframe html
							$video = get_sub_field('video');
							//so lets use regex to grab just anything inside the source
							preg_match('/src="(.+?)"/', $video, $videosrc); //this returns the array of $videosrc containing the source of the video
							
							
						}
					}
					
				}
			}
		}
/*
				//load the linked gallery on the post
				$attachgallery  = get_field('attach_gallery');
				foreach($attachgallery as $post) {
					//setup the post with all of the media elements 
 					setup_postdata($post);
 					
 					//get images
 					$images = get_field('images');
 					print_r($images);
 					//get videos
 					if( have_rows('videos') ) {
	 					echo "Yes";
	 					while(have_rows('videos')) {
		 					the_row();
		 					$video = get_sub_field('video');
		 					preg_match('/src="(.+?)"/', $video, $videosrc);
		 					print_r($videosrc);
	 					}
 					}
					
 				}
*/
	}
}


/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function vermeer_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'vermeer_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'vermeer_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so vermeer_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so vermeer_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in vermeer_categorized_blog.
 */
function vermeer_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'vermeer_categories' );
}
add_action( 'edit_category', 'vermeer_category_transient_flusher' );
add_action( 'save_post',     'vermeer_category_transient_flusher' );
